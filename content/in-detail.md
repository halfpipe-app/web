---
title: "Halfpipe: In Detail"
layout: in-detail
learningVideos:
    sectionTitle: Learn Halfpipe
    sectionSubtitle: How it works in four short videos
    videos:
    - title: "1 - Start Here"
      subtitle: "One-off Migration (2 mins)"
      link: https://www.youtube.com/embed/ZezHkwluCcs
    - title: 2 - Keep Up-To-Date
      subtitle: "Continuous Migration (6 mins)"
      link: https://www.youtube.com/embed/-xqIKgi7Af4
    - title: 3 - Data Integration as a Microservice
      subtitle: "DIaaMs, Is That A thing? (2 mins)"
      link: https://www.youtube.com/embed/Ckt_fTceh5I
    - title: 4 - Validate Your Data
      subtitle: "Report On The Differences Between Tables (2 mins)"
      link: https://www.youtube.com/embed/rGakMNs2dTE
getStartedVideos:
    sectionTitle: How To Get Started
    sectionSubtitle: "From zero to hero. Get running with Halfpipe in 2 mins"
    videos:
    - title: How To Run Halfpipe
      subtitle: A Quickstart Guide (2 mins)
      link: https://youtube.com/embed/OKpQ9nMNdSk
---
