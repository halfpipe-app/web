---
title: "Halfpipe: About The Creator"
subtitle: 
layout: about-the-creator
caseStudyText: 
heading: About The Creator
image: images/richard-portrait-1.jpeg
altText: Image of Richard Lloyd
blurb: 
    intro: "Hi, I'm Richard Lloyd"
    tagLine: '"I’m a passionate believer in getting the job done, using tools that just work."'
    main: | 
        I have 15 years' experience engineering data integration platforms, Oracle & Snowflake databases, Linux, Golang, Python and Docker. 
        <br><br>
        After [painful experiences](/blog/01-oracle-to-snowflake-using-pentaho-and-why-you-shouldnt-use-it/) using Pentaho to migrate to Snowflake, I knew it’d be far simpler if there was a tool that wrapped up the ETL functionality and data integration patterns that you need to write in-house.
        <br><br>
        So I created Halfpipe. A simple tool that helps every engineer move records between RDBMS, Snowflake and S3. It reduces all of the moving parts to just one command.
        <br><br>
        To find out more about me, head over to my [LinkedIn profile](https://www.linkedin.com/in/rich4rdlloyd), check out my [GitHub](https://github.com/relloyd/halfpipe) or [blog](/blog). 
nextStepsText: To learn more about Halfpipe, I have a short video (2min) for you. Click below to get started.
buttonText: Learn More About Halfpipe >>>
redirectTo: enterprise/video.md
useSocialMediaDisclaimer: true
isLandingPage: True
---
