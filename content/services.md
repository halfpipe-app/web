---
title: "Halfpipe: Services"
subtitle:
layout: services
heading: How To Buy
subtitle: |
  Cut down on the run cost of your data integration teams by saving them time with Halfpipe.
  <br><br>
  Pay as you go by using the Halfpipe [AWS machine image](https://aws.amazon.com/marketplace/pp/B08PL1STFS?qid=1610988617064&sr=0-1&ref_=srh_res_product_title) or choose a package that works for you.
footerTitle: How To Learn More Before You Buy
footerText: |
  <br><br>
  Try Halfpipe for free via [GitHub](https://github.com/relloyd/halfpipe).
  <br><br>
  Drop me a line or schedule a call using the [Contact page](/contact).
options:
  - planHeader: |
      Migrate <br>
      To Snowflake 
    planItems:
      - Rapid migration programme
      - Work with your existing data teams to deploy Halfpipe
      - Use on-prem, existing or new cloud infrastructure
      - Support and maintenance
    buttonText: Get Started >>>
    buttonLink: /contact
  - planHeader: |
      Data Platform <br>
      Design & Build 
    planItems:
      - Design & build a DataOps platform
      - Recruit a high quality team to run it 
      - Use on-prem, existing or new cloud infrastructure
      - Support and maintenance
    buttonText: Get Started >>>
    buttonLink: /contact
  - planHeader: |
      Data Platform <br>
      Assessment
    planItems:
      - Data platform scalability and cost review
      - Team operations and scalability review
      - DataOps readiness report
      - "--"
    buttonText: Get Started >>>
    buttonLink: /contact
---