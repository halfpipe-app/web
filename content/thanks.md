---
title: "Halfpipe: Thank You"
subtitle: Your Application Is Complete!
---

Hi,

Thanks for scheduling a time to speak with me and completing the survey, I look forward to speaking with you soon.

__A Few Notes__

* I've sent you confirmation of your call's time and date via email, please make sure that you block this in your calendar
* I'll call you via Skype or phone (depending on what you selected in the survey) at the precise time that you selected
* So we can talk easily, please make sure you're ready in a quiet room, somewhere without background noise or use headphones with a tried and tested mic  

__Preparing__

All you need to do to prepare for our call is get an idea of where you want to go and what you want to achieve.  

On the call, we will look at your current situation and where you want to take your business. Then we'll map out an action plan to get you there.

<br>

I look forward to helping you accelerate your data-ops and achieving success!

*-- Richard*
