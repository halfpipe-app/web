---
title: "Halfpipe: Copy Table Changes / Deltas"
blogTitle: Copy Table Changes / Deltas
subtitle: Easily update target tables in one command
image: noun_change_1766505.png
altText: Image of Change by Adrien Coquet from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### Copy Table Changes / Deltas From Oracle To Snowflake

{{< youtube-video "https://www.youtube.com/embed/5qbsULxJVtk" >}}

#### Copy Table Changes / Deltas From Oracle To S3

{{< youtube-video "https://www.youtube.com/embed/-rQvx1Z-A8w" >}}
