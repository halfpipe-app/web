---
title: "Halfpipe: Connections"
blogTitle: Create Connections
subtitle: Securely add Oracle, Snowflake and S3 targets
image: noun_connection_1264451.png
altText: Image of Connection by Becris from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

#### How To Create Connections

Connection details are stored securely at rest using industry standard AES-256 encryption.

{{< youtube-video "https://www.youtube.com/embed/kygUOv8c8_c" >}}
