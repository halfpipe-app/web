---
title: "Halfpipe: Copy Table Snapshots"
blogTitle: Copy Table Snapshots
subtitle: Migrate a table with all its columns in one command
image: noun_Camera_2663269.png
altText: Image of Camera by Mardeli Darso from the Noun Project
breadcrumbs:
    home: 
        name: Back To Features Home
        link: /features
---

In the demos below, all columns of a sample table are copied from source to target.

If we need to __edit the columns__ that are migrated, we can generate a pipeline config file and edit some SQL. 

Here's a video showing [how to generate pipeline config files](../pipes).

<br>

#### Copy A Snapshot From Oracle To Snowflake

{{< youtube-video "https://www.youtube.com/embed/3wX5Nr41giA" >}}

#### Copy A Snapshot From Oracle To S3

{{< youtube-video "https://www.youtube.com/embed/im0fD4sYvSA" >}}

#### Copy A Snapshot From Oracle To Oracle

{{< youtube-video "https://www.youtube.com/embed/ZND4Flsux58" >}}
